<cfcomponent extends="Base">
    <cffunction name="returnVendorID" returntype="string" access="private">
        <cfreturn "keyscaper">
    </cffunction>

    <cffunction name="returnSecretKey" returntype="string" access="private">
        <cfreturn "7d26ae8acc55b13dad9e80a4bde40106">
    </cffunction>

    <cffunction name="returnHash" returntype="string" access="private">
        <cfset hashInput = returnVendorID() & returnSecretKey()>
        <cfreturn hash(hashInput)>
    </cffunction>

    <cffunction name="getOrders" access="remote" returntype="xml">
        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=listneworders&vendorid=#returnVendorID()#&hash=#returnHash()#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfdump var="#XMLParse(cfhttp.fileContent)#">
        <cfreturn #XMLParse(cfhttp.fileContent)#>
    </cffunction>

    <cffunction name="getUpdatedOrders" access="remote">
        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=listupdatedorders&vendorid=#returnVendorID()#&hash=#returnHash()#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfreturn #cfhttp#>
    </cffunction>

    <cffunction name="acknowledgeOrder">
        <cfargument name="OrderID">
        <cfargument name="type">
        <cfargument name="action" default="accept">

        <cfset hashInput = returnVendorID & ARGUMENTS.OrderID & ARGUMENTS.type & returnSecretKey>
        <cfset ackHash = hash(hashInput)>

        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=ackorder&vendorid=#returnVendorID#&OrderID=#ARGUMENTS.OrderID#&type=#ARGUMENTS.type#&hash=#ackHash#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfreturn #cfhttp#>
    </cffunction>

    <cffunction name="getOrderMessages">
        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=listordermessages&vendorid=#returnVendorID#&hash=#returnHash#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfreturn #cfhttp#>
    </cffunction>

    <cffunction name="listOrdersExample" access="remote">
        <cfset returnVendorID = "keyscaper">
        <cfset returnSecretKey = "7d26ae8acc55b13dad9e80a4bde40106">
        <cfset hashInput = returnVendorID & returnSecretKey>
        <cfset returnHash = hash(hashInput)>

        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=listneworders&vendorid=#returnVendorID#&hash=#returnHash#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfset XMLDoc  = #XMLParse(cfhttp.fileContent)#>
        
        <cfif isDefined("XMLDoc.Response.Result.Orders.Order")>
            <cfloop array="#XMLDoc.Response.Result.Orders.Order#" index="o">

                <cfquery name="checkOrder">
                    SELECT OrderID
                    FROM TempOrders
                    WHERE OrderID = '#o.OrderID.xmltext#'
                </cfquery>

                <cfif checkOrder.recordCount EQ 0>
                    <cfquery name="insertTempOrder">
                        INSERT INTO TempOrders (OrderID, OrderDate, OrderType, DeliveryMethod, Priority, Currency, Status, Attributes, Address1, Address2, Address3, Name, Name2, City,
                            State, Country, CountryCode, Zip, Phone, Email, Type)
                        VALUES ('#o.OrderID.xmltext#', '#o.OrderDate.xmltext#','#o.OrderType.xmltext#','#o.DeliveryMethod.xmltext#','#o.Priority.xmltext#','#o.Currency.xmltext#','#o.Status.xmltext#',
                            '#o.Attributes.xmltext#','#o.ShippingAddress.Address1.xmltext#','#o.ShippingAddress.Address2.xmltext#','#o.ShippingAddress.Address3.xmltext#','#o.ShippingAddress.Name.xmltext#','#o.ShippingAddress.Name2.xmltext#','#o.ShippingAddress.City.xmltext#','#o.ShippingAddress.State.xmltext#','#o.ShippingAddress.Country.xmltext#',
                            '#o.ShippingAddress.CountryCode.xmltext#','#o.ShippingAddress.Zip.xmltext#','#o.ShippingAddress.Phone.xmltext#','#o.ShippingAddress.Email.xmltext#','#o.ShippingAddress.Type.xmltext#')
                    </cfquery>
                    <cfloop array="#o.LineItems.LineItem#" index="OrderLine">
                        <cfquery name="insertTempOrderLine">
                            INSERT INTO TempOrderLine (LineItemID, OrderID, LineItemType, Quantity, Description, Attributes, VendorAttributes, 
                            Price, ProductID)
                            VALUES ('#OrderLine.LineItemID.xmltext#','#OrderLine.OrderID.xmltext#','#OrderLine.LineItemType.xmltext#','#OrderLine.Quantity.xmltext#','#OrderLine.Description.xmltext#',
                                '#OrderLine.Attributes.xmltext#','#OrderLine.VendorAttributes.xmltext#','#OrderLine.Price.xmltext#','#OrderLine.ProductID.xmltext#')
                        </cfquery>
                        <cfloop array="#OrderLine.PrintFiles.PrintFile#" index="pf">
                            <!---<cfset tempTitle = "#CreateUUID()#.pdf">--->
                            <cfset tempTitle = "#OrderLine.LineItemType.xmltext#-#o.OrderID.xmltext#-#OrderLine.LineItemID.xmltext#.pdf">
                            <cfset fileRes = getPrintFiles("#pf.URL.xmltext#","#pf.Type.xmltext#","#tempTitle#","PrintFiles","#o.OrderID.xmltext#")>
                            <cfquery name="insertPrintFile">
                                INSERT INTO TempPrintFiles (LineItemID, Type, URL, Description, PrintFileName)
                                VALUES ('#OrderLine.LineItemID.xmltext#','#pf.Type.xmltext#','#pf.URL.xmltext#','#pf.Description.xmltext#','#tempTitle#')
                            </cfquery>
                        </cfloop>
                        <cfloop array="#OrderLine.Previews.PreviewFile#" index="PrevFile">
                            <cfset tempTitle = "#CreateUUID()#.jpg">
                            <cfset fileRes = getFiles("#PrevFile.URL.xmltext#","#PrevFile.Type.xmltext#","#tempTitle#","PreviewFiles","#o.OrderID.xmltext#")>
                            <cfquery name="insertPreviewFile">
                                INSERT INTO TempPreviews (LineItemID, Type, URL, Description,PreviewFileName)
                                VALUES ('#OrderLine.LineItemID.xmltext#','#PrevFile.Type.xmltext#','#PrevFile.URL.xmltext#','#PrevFile.Description.xmltext#','#tempTitle#')
                            </cfquery>
                        </cfloop>
                        <cfloop array="#o.PackingSheet.Page#" index="PS">
                            <cfset inc = 1>
                            <cfloop array="#PS.Front#" index="PSDetails">
                                <cfset fname = "#o.OrderID.xmltext#-#inc#">
                                <cfset fileRes = getShippingDocs("#PSDetails.URL.xmltext#","#PSDetails.Type.xmltext#","#fname#.jpg")>
                                <cfquery name="ps">
                                    INSERT INTO TempPackingSheet (TempOrderID, PageNumber, Type, Description, URL)
                                    VALUES ('#o.OrderID.xmltext#','#PS.PageNumber.xmltext#','#PSDetails.Type.xmltext#','#PSDetails.Description.xmltext#','#PSDetails.URL.xmltext#')
                                </cfquery>
                                <cfset inc++>
                            </cfloop>
                        </cfloop>
                    </cfloop>

                    <!---acknowledge the order--->
                    <!---<cfset hashInput = returnVendorID & o.OrderID.xmltext & "new" & returnSecretKey>
                    <cfset ackHash = hash(hashInput)>
                    <cfset ackURL = "https://vendor.zazzle.com/v100/api.aspx?method=ackorder&vendorid=#returnVendorID#&orderid=#o.OrderID.xmltext#&type=new&action=accept&hash=#ackHash#">
                    <cfhttp url="#ackURL#" charset="utf-8">--->

                    <!---update the record--->
                    <cfquery name="updateRec">
                        UPDATE TempOrders
                        SET OrderAcknowledged = 1
                        WHERE OrderID = #o.OrderID.xmltext#
                    </cfquery>
                </cfif>
            </cfloop>
        </cfif>
    </cffunction>

    <cffunction name="getShippingLabel" access="remote" returnformat="JSON">
        <cfargument name="orderid" required="true">
        <cfargument name="weight" required="true">

        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=getshippinglabel&vendorid=#returnVendorID()#&orderid=#ARGUMENTS.orderID#&weight=#ARGUMENTS.weight#&format=ZPL&hash=#returnHash()#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfset XMLDoc  = #XMLParse(cfhttp.fileContent)#>

        <cfquery name="upOrder">
            UPDATE TempOrders
            SET ShippingCarrier = '#XMLDoc.Response.Result.ShippingInfo.Carrier.xmltext#',
            ShippingMethod = '#XMLDoc.Response.Result.ShippingInfo.Method.xmltext#',
            TrackingNumber = '#XMLDoc.Response.Result.ShippingInfo.TrackingNumber.xmltext#',
            Weight = '#XMLDoc.Response.Result.ShippingInfo.Weight.xmltext#',
            KS_ProductionStatus = 'In Production',
            KS_ProductionStatusDate = #createODBCDateTime(now())#
            WHERE OrderID = '#ARGUMENTS.orderid#'
        </cfquery>

        <Cfset retFileName = "">
        <cfloop array="#XMLDoc.Response.Result.ShippingInfo.ShippingDocuments.ShippingDocument#" index="o">
            <cfset fileName = "#o.type.xmltext#-#ARGUMENTS.orderID#.#o.format.xmltext#">
            <Cfset retfileName &= "#o.type.xmltext#-#ARGUMENTS.orderID#.#o.format.xmltext#,">
            <cfhttp url="#o.URL.xmltext#" method="get" getasbinary="yes" path="#ExpandPath("..")#/ZazzleFiles/ShippingLabels/" file="#fileName#">
        </cfloop>

        <cfreturn retfileName />

    </cffunction>

    <cffunction name="getFiles">
        <cfargument name="URL">
        <cfargument name="fileType">
        <cfargument name="fileName">
        <cfargument name="folderName">
        <cfargument name="orderID">

        <cfif NOT directoryExists("#ExpandPath("..")#/ZazzleFiles/#ARGUMENTS.folderName#/#ARGUMENTS.OrderID#")>
            
            <cfdirectory    action="create"
                            directory = "#ExpandPath("..")#/ZazzleFiles/#ARGUMENTS.folderName#/#ARGUMENTS.OrderID#">
        </cfif>
                            


        <cfhttp url="#ARGUMENTS.URL#" method="get" getasbinary="yes" path="#ExpandPath("..")#/ZazzleFiles/#ARGUMENTS.folderName#/#ARGUMENTS.OrderID#/" file="#ARGUMENTS.fileName#">

        <cfreturn "success">
    </cffunction>

    <cffunction name="getPrintFiles">
        <cfargument name="URL">
        <cfargument name="fileType">
        <cfargument name="fileName">

        <cfhttp url="#ARGUMENTS.URL#" method="get" getasbinary="yes" path="#ExpandPath("..")#/ZazzleFiles/PrintFiles/Incoming/" file="#ARGUMENTS.fileName#">

        <cfset pfName = replace("#ARGUMENTS.fileName#",".pdf",".jpg", "all")>

        <!---convert this thing--->
        <cfset convertBat = "convert -density 300 #ExpandPath("..")#/ZazzleFiles/PrintFiles/Incoming/#ARGUMENTS.fileName# #ExpandPath("..")#/ZazzleFiles/PrintFiles/Processed/#pfName#">
        <cffile action="write" file="#ExpandPath("..")#/ZazzleFiles/PrintFiles/Incoming/convert.bat" output='#convertBat#'>
        <cfexecute name="#ExpandPath("..")#/ZazzleFiles/PrintFiles/Incoming/convert.bat" timeout="0"/>
    </cffunction>

    <cffunction name="getShippingDocs">
        <cfargument name="URL">
        <cfargument name="fileType">
        <cfargument name="fileName">

        <cfhttp url="#ARGUMENTS.URL#" method="get" getasbinary="yes" path="#ExpandPath("..")#/ZazzleFiles/ShippingDocuments/" file="#ARGUMENTS.fileName#">
    </cffunction>

    <cffunction name="getAllZazzleOrders" returntype="Query">
        <cfargument name="orderBy" required="false" default="" />
        <cfargument name="orderDirection" required="false" default="" />
        <cfargument name="filters" required="false" default="" />

        <!--- adding a space to order id because CF will automatically translate this to numeric when converting to JSON. space overrides this to string --->
        <cfquery name="qry">
            SELECT TempOrderID, ' ' + OrderID as OrderID, Quantity, OrderDate, Name, FormFactor, Status, KS_ProductionStatus, KS_ProductionStatusDate, BatchNumber, DeliveryMethod, PreviewFileName, PrintFileName
            FROM ZazzleOrderview
            WHERE OrderAcknowledged = 1
            <cfif ARGUMENTS.filters NEQ "">
                #preserveSingleQuotes(ARGUMENTS.filters)#
            </cfif>
            <!---ORDER BY OrderCreatedDate DESC--->
        </cfquery>

        <cfreturn qry />
    </cffunction>

    <cffunction name="getZazzleProductionListing_JSON" returnformat="JSON" access="remote">
        <cfargument name="colList" required="yes" hint="list of columns to return (this minimizes data transfer overhead)">
        <cfargument name="filter" required="no" hint="key:value pair list of filter values" default="">

        <cfset filterStatment = "">

        <cfset OrderRes = getAllZazzleOrders('','',"AND OrderAcknowledged = 1 AND (KS_ProductionStatus is null OR KS_ProductionStatus = '')")>

        <cfset OrderData = StructNew()>
        <cfset OrderData['aaData'] = arrayNew(1)>
        <cfloop query="OrderRes">
            <cfset OrderRow = structNew()>
            <cfif listFindNoCase(ARGUMENTS.collist, "OrderID") EQ 0>
                <cfset OrderRow["orderid"] = JSStringFormat(OrderRes["orderid"][OrderRes.currentRow])>
            </cfif>
            <cfloop list="#ARGUMENTS.colList#" index="thisCol">
                <cfif thisCol EQ "OrderID">
                    <cfset OrderRow["#thisCol#"] = #OrderRes["#thisCol#"][#OrderRes.currentRow#]#>
                <cfelse>
                    <cfset OrderRow["#thisCol#"] = JSStringFormat(OrderRes["#thisCol#"][#OrderRes.currentRow#])>
                </cfif>
                
            </cfloop>
            <cfset arrayAppend(OrderData["aaData"], OrderRow)>
        </cfloop>

        <cfreturn OrderData />
    </cffunction>

    <cffunction name="getZazzleShippingListing" returnformat="JSON" access="remote">
        <cfquery name="getSL">
            SELECT TempOrderID, ' ' + OrderID as OrderID, OrderDate, DeliveryMethod, Status, Address1, Address2, Address3, Name, Name2, City,
            State, Country, Zip, Phone, KS_ProductionStatus, KS_ProductionStatusDate, KS_ProductionBatchNumber
            FROM TempOrders
            WHERE KS_ProductionStatus = 'In Production'
            ORDER BY KS_ProductionStatusDate DESC
        </cfquery>

        <cfreturn getSL />
    </cffunction>

    <cffunction name="getZazzleShippingListing_JSON" returnformat="JSON" access="remote">
        <cfargument name="colList" required="yes" hint="list of columns to return (this minimizes data transfer overhead)">
        <cfargument name="filter" required="no" hint="key:value pair list of filter values" default="">

        <cfset filterStatment = "">

        <cfset OrderRes = getZazzleShippingListing()>

        <cfset OrderData = StructNew()>
        <cfset OrderData['aaData'] = arrayNew(1)>
        <cfloop query="OrderRes">
            <cfset OrderRow = structNew()>
            <cfif listFindNoCase(ARGUMENTS.collist, "OrderID") EQ 0>
                <cfset OrderRow["orderid"] = JSStringFormat(OrderRes["orderid"][OrderRes.currentRow])>
            </cfif>
            <cfloop list="#ARGUMENTS.colList#" index="thisCol">
                <cfif thisCol EQ "OrderID">
                    <cfset OrderRow["#thisCol#"] = #OrderRes["#thisCol#"][#OrderRes.currentRow#]#>
                <cfelse>
                    <cfset OrderRow["#thisCol#"] = JSStringFormat(OrderRes["#thisCol#"][#OrderRes.currentRow#])>
                </cfif>
                
            </cfloop>
            <cfset arrayAppend(OrderData["aaData"], OrderRow)>
        </cfloop>

        <cfreturn OrderData />
    </cffunction>



    <cffunction name="createGrids" access="remote" returnformat="JSON">
        <cfargument name="fileArray">
        <!---<cfset code = Left("#ListFirst(ARGUMENTS.FileArray)#", 6)>--->
        <cfset rootdir = "#ExpandPath("..")#\ZazzleFiles\PrintFiles\Processed\">
        <cfset gridfiledir = "#ExpandPath("..")#\ZazzleFiles\Grids\Batch\">
        <cfset completedgridfiledir = "#ExpandPath("..")#\ZazzleFiles\Grids\Completed\">

        <!---get the batch number--->
        <cfquery name="getBatch">
            SELECT MAX(KS_ProductionBatchNumber) BN FROM TempOrders
        </cfquery>

        <cfif getBatch.BN NEQ "">
            <cfset thisBN = getBatch.BN+1>
        <cfelse>
            <cfset thisBN = 1>
        </cfif>
        
        <cfset imageList = "">
        <cfset prevCode = "">
        <cfset prevDimensions = "">
        <cfset code = "">
        <cfset batchfiledir = "Zazzle_#dateFormat(now(), 'yyyy-mm-dd')#_#timeFormat(now(), 'hhmm')#">
        <cfset gfd = #gridfiledir#&#batchfiledir#>
        <cfdirectory action="create" directory="#gfd#">
        <cfdirectory action="create" directory="\\192.168.100.10\Operations\Keyscaper\Grid Print Files\Active Orders\#batchfiledir#">
        
        <cfset arguments.filearray = listSort(#ARGUMENTS.filearray#, "textnocase")>
        <cfloop list="#ARGUMENTS.fileArray#" index="i">
            
            <!---get the code--->
            <cfswitch expression="#listFirst(i,"_")#">
                <cfcase value="keyscapermouse">
                    <cfset code = "K110MS">
                </cfcase>
                <cfcase value="keyscaperheadphones">
                    <cfset code = "KCANI3">
                </cfcase>
                <cfcase value="keyscaperpowerbank">
                    <cfset code = "KPBK4K">
                </cfcase>
            </cfswitch>
            
            <cfquery name="getDimensions">
                SELECT GridDimensionX, GridDimensionY
                FROM FormFactors
                WHERE Code = '#code#'
            </cfquery>

            <cfif code NEQ prevCode>
                <cffile action="write" file="#gfd#\#prevcode#args.txt" output="#imageList#">
                <cffile action="write" file="#gfd#\proc#prevcode#.bat" output='montage -geometry 100%% -tile #prevDimensions# @#gfd#\#prevCode#args.txt "\\192.168.100.10\Operations\Keyscaper\Grid Print Files\Active Orders\#batchfiledir#\#prevCode#.jpg"'>
                <cfexecute name="#gfd#/proc#prevcode#.bat" timeout="0"/>
                <cfset imagelist = "">
            </cfif>

            <!---calc white files--->
            <cfset totalImg = listLen("#ARGUMENTS.fileArray#")>

            <cfquery name="getOID">
                SELECT tor.OrderID, tol.Quantity
                FROM TempOrders tor
                INNER JOIN TempOrderLine tol on tol.OrderID = tor.OrderID
                INNER JOIN TempPrintFiles tpf on tpf.LineItemID = tol.LineItemID
                WHERE tpf.printFileName = '#i#'
            </cfquery>
            <!---set the status and create a batch--->
            <cfquery name="updateStatus">
                UPDATE TempOrders
                SET KS_ProductionStatus = 'In Production',
                KS_ProductionStatusDate = #createODBCDateTime(now())#,
                KS_ProductionBatchNumber = '#thisBN#'
                WHERE OrderID = '#getOID.OrderID#'
            </cfquery>
            <cfloop from="1" to="#getOID.quantity#" index="z">
                <cfset imageList &= " #replace('#rootdir##i#','pdf','jpg')#">
            </cfloop>

            <cfset prevCode = code>
            <cfset prevDimensions = "#getDimensions.GridDimensionX#x#getDimensions.GridDimensionY#">
        </cfloop>

        <cffile action="write" file="#gfd#\#code#args.txt" output="#imageList#">
        <cffile action="write" file="#gfd#\proc#code#.bat" output='montage -geometry 100%% -tile #getDimensions.GridDimensionX#x#getDimensions.GridDimensionY# @#gfd#\#code#args.txt "\\192.168.100.10\Operations\Keyscaper\Grid Print Files\Active Orders\#batchfiledir#\#code#.jpg"'>
        <cfexecute name="#gfd#/proc#code#.bat" timeout="0"/>

        <cfreturn #batchfiledir# />
    </cffunction>

    <cffunction name="getItemsTable" access="remote" returnformat="JSON">
        <cfargument name="orderID" required="true">

        <cfquery name="getItems">
            select Quantity, tol.orderid + '/' + PreviewFileName as filepath 
            from temporderline tol
            inner join TempPreviews tp on tp.LineItemID = tol.LineItemID
            where tol.orderID = #mid(ARGUMENTS.orderID,3,100)#
        </cfquery>

        <cfreturn getItems />
    </cffunction>

    <cffunction name="updateZazzleOrders" access="remote">
        <cfset returnVendorID = "keyscaper">
        <cfset returnSecretKey = "7d26ae8acc55b13dad9e80a4bde40106">
        <cfset hashInput = returnVendorID & returnSecretKey>
        <cfset returnHash = hash(hashInput)>

        <cfset theURL = "https://vendor.zazzle.com/v100/api.aspx?method=listupdatedorders&vendorid=#returnVendorID#&hash=#returnHash#">
        <cfhttp url="#theURL#" charset="utf-8">
        <cfset XMLDoc  = #XMLParse(cfhttp.fileContent)#>

        <cfif isDefined("XMLDoc.Response.Result.Orders.Order")>
            <cfloop array="#XMLDoc.Response.Result.Orders.Order#" index="o">
                <!---if order is to be removed--->
                <cfif o.update.updateType.xmltext EQ "Remove">
                    <cfset OrderID = o.OrderID.xmltext>

                    <cfquery name="delOrder">
                        UPDATE TempOrders
                        SET OrderAcknowledged = 0,
                        KS_ProductionStatus = 'Deleted',
                        KS_ProductionStatusDate = #createODBCDateTime(now())#
                        WHERE OrderID = '#OrderID#'
                    </cfquery>
                <cfelseif o.update.updateType.xmltext EQ "ShippingInfo">
                    <cfquery name="upOrder">
                        UPDATE TempOrders
                        SET Address1 = '#o.ShippingAddress.Address1.xmltext#',
                        Address2 = '#o.ShippingAddress.Address2.xmltext#',
                        Address3 = '#o.ShippingAddress.Address3.xmltext#',
                        Name = '#o.ShippingAddress.Name.xmltext#',
                        Name2 = '#o.ShippingAddress.Name2.xmltext#',
                        City = '#o.ShippingAddress.City.xmltext#',
                        State = '#o.ShippingAddress.State.xmltext#',
                        Country = '#o.ShippingAddress.Country.xmltext#',
                        CountryCode = '#o.ShippingAddress.CountryCode.xmltext#',
                        Zip = '#o.ShippingAddress.Zip.xmltext#',
                        Phone = '#o.ShippingAddress.Phone.xmltext#',
                        Email = '#o.ShippingAddress.Email.xmltext#',
                        Type = '#o.ShippingAddress.Type.xmltext#'
                        WHERE OrderID = '#OrderID#'
                    </cfquery>
                <cfelseif o.update.updateType.xmltext EQ "ProductInfo">
                    <!---probably delete the entire order and re-add it--->
                    <cfquery name="delPrevs">
                        DELETE FROM TempPreviews
                        WHERE LineItemID IN (SELECT LineItemID FROM TempOrderLine WHERE OrderID = '#OrderID#')
                    </cfquery>
                    <cfquery name="delPrintFiles">
                        DELETE FROM TempPrintFiles
                        WHERE LineItemID IN (SELECT LineItemID FROM TempOrderLine WHERE OrderID = '#OrderID#')
                    </cfquery>
                    <cfquery name="delPackingSheets">
                        DELETE FROM TempPackingSheet
                        WHERE TempOrderID = '#OrderID#'
                    </cfquery>
                    <cfquery name="delLines">
                        DELETE FROM TempOrderLine
                        WHERE OrderID = '#OrderID#'
                    </cfquery>
                    <!---<cfquery name="delOrders">
                        DELETE FROM TempOrders
                        WHERE OrderID = '#OrderID#'
                    </cfquery>--->

                    <!---delete packing sheet--->
                    <cfset path = "#ExpandPath("..")#\glados\ZazzleFiles\ShippingDocuments\#OrderID#-*">
                    <cffile action="write" file="#ExpandPath("..")#\glados\ZazzleFiles\ShippingDocuments\del.bat" output="del #path#" nameconflict="overwrite">
                    <cfexecute name="#ExpandPath("..")#\glados\ZazzleFiles\ShippingDocuments\del.bat" timeout="0">
                        

                    <cfquery name="insertTempOrderLine">
                        INSERT INTO TempOrderLine (LineItemID, OrderID, LineItemType, Quantity, Description, Attributes, VendorAttributes, 
                        Price, ProductID)
                        VALUES ('#OrderLine.LineItemID.xmltext#','#OrderLine.OrderID.xmltext#','#OrderLine.LineItemType.xmltext#','#OrderLine.Quantity.xmltext#','#OrderLine.Description.xmltext#',
                            '#OrderLine.Attributes.xmltext#','#OrderLine.VendorAttributes.xmltext#','#OrderLine.Price.xmltext#','#OrderLine.ProductID.xmltext#')
                    </cfquery>
                    <cfloop array="#OrderLine.PrintFiles.PrintFile#" index="pf">
                        <!---delete existing print file--->
                        <cfset tempTitle = "#OrderLine.LineItemType.xmltext#-#o.OrderID.xmltext#-#OrderLine.LineItemID.xmltext#.pdf">
                        <cfset pfTitle = "#OrderLine.LineItemType.xmltext#-#o.OrderID.xmltext#-#OrderLine.LineItemID.xmltext#.jpg">
                        <cffile action="delete" file="#ExpandPath("..")#/ZazzleFiles/PrintFiles/Incoming/#tempTitle#">
                        <cffile action="delete" file="#ExpandPath("..")#/ZazzleFiles/PrintFiles/Processed/#pfTitle#">
                        <cfset fileRes = getPrintFiles("#pf.URL.xmltext#","#pf.Type.xmltext#","#tempTitle#","PrintFiles","#o.OrderID.xmltext#")>
                        <cfquery name="insertPrintFile">
                            INSERT INTO TempPrintFiles (LineItemID, Type, URL, Description, PrintFileName)
                            VALUES ('#OrderLine.LineItemID.xmltext#','#pf.Type.xmltext#','#pf.URL.xmltext#','#pf.Description.xmltext#','#tempTitle#')
                        </cfquery>
                    </cfloop>
                    <cfloop array="#OrderLine.Previews.PreviewFile#" index="PrevFile">
                        <cfset tempTitle = "#CreateUUID()#.jpg">
                        <cfset fileRes = getFiles("#PrevFile.URL.xmltext#","#PrevFile.Type.xmltext#","#tempTitle#","PreviewFiles","#o.OrderID.xmltext#")>
                        <cfquery name="insertPreviewFile">
                            INSERT INTO TempPreviews (LineItemID, Type, URL, Description,PreviewFileName)
                            VALUES ('#OrderLine.LineItemID.xmltext#','#PrevFile.Type.xmltext#','#PrevFile.URL.xmltext#','#PrevFile.Description.xmltext#','#tempTitle#')
                        </cfquery>
                    </cfloop>
                    <cfloop array="#o.PackingSheet.Page#" index="PS">
                        <cfset inc = 1>
                        <cfloop array="#PS.Front#" index="PSDetails">
                            <cfset fname = "#o.OrderID.xmltext#-#inc#">
                            <cfset fileRes = getShippingDocs("#PSDetails.URL.xmltext#","#PSDetails.Type.xmltext#","#fname#.jpg")>
                            <cfquery name="ps">
                                INSERT INTO TempPackingSheet (TempOrderID, PageNumber, Type, Description, URL)
                                VALUES ('#o.OrderID.xmltext#','#PS.PageNumber.xmltext#','#PSDetails.Type.xmltext#','#PSDetails.Description.xmltext#','#PSDetails.URL.xmltext#')
                            </cfquery>
                            <cfset inc++>
                        </cfloop>
                    </cfloop>
                <cfelseif o.update.updateType.xmltext EQ "Priority">
                    <!---we do not care about this at all--->
                </cfif>

                <!---acknowledge these things--->
                <cfset hashInput = returnVendorID & o.OrderID.xmltext & "update" & returnSecretKey>
                <cfset ackHash = hash(hashInput)>
                <cfset ackURL = "https://vendor.zazzle.com/v100/api.aspx?method=ackorder&vendorid=#returnVendorID#&orderid=#o.OrderID.xmltext#&type=update&action=accept&hash=#ackHash#">
                <cfhttp url="#ackURL#" charset="utf-8">
            </cfloop>
        </cfif>
    </cffunction>
</cfcomponent>