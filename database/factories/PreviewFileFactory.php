<?php

use Faker\Generator as Faker;

$factory->define(App\PreviewFile::class, function (Faker $faker) {
    return [
        'Url' => $faker->imageUrl(),
    ];
});
