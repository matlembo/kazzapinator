@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="/orders/download-print-files" id="download-print-files-form">
            @csrf
            <div class="row">
                <div class="col">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col text-right">
                    @if($orders->count())
                        <div class="form-group">
                            <input class="btn btn-primary" type="submit" value="Download Print Files for Selected Orders" />
                            <small class="form-text text-muted">This will set the selected Orders' Status to "IN-PRODUCTION"</small>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Zazzle Order ID</th>
                            <th>Previews</th>
                            <th>Ship To</th>
                            <th>Date</th>
                            <th>Delivery</th>
                            <th>Zazzle Status</th>
                            <th>KS Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="ids[]" value="{{ $order->id }}" checked />
                                    </div>
                                </td>
                                <td><a href="/orders/{{ $order->id }}/edit">{{ $order->OrderId }}</a></td>
                                <td>
                                    @foreach ($order->LineItems as $item)
                                        @foreach ($item->PreviewFiles as $file)
                                            <img class="preview-image" src="{{ asset($file->path) }}" />
                                        @endforeach
                                    @endforeach
                                </td>
                                <td>
                                    {{ $order->Name }}, {{ $order->Phone }}<br />
                                    <small>
                                        {{ $order->Address1 }}<br />
                                        {{ $order->State }}, {{ $order->Zip }}<br />
                                        {{ $order->Country }}
                                    </small>
                                </td>
                                <td>{{ \Carbon\Carbon::parse($order->OrderDate)->toDayDateTimeString() }}</td>
                                <td>{{ $order->DeliveryMethod }}</td>
                                <td>{{ $order->Status }}</td>
                                <td>{{ $order->ks_status }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
@endsection
