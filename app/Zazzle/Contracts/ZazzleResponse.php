<?php

namespace App\Services\Zazzle\Contracts;

interface ZazzleResponse {

    public function setXml();

    public function getXml();

    public function getId();

    public function parseData();

}