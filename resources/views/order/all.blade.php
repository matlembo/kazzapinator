@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>All Orders</h1>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Order</th>
                        <th>Ship To</th>
                        <th>Items</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>
                                <a href="/orders/{{ $order->id }}/edit">{{ $order->OrderId }}</a>
                                <p class="order-date">{{ \Carbon\Carbon::parse($order->OrderDate)->toDayDateTimeString() }}</p>
                                <p>
                                    KS: {{ $order->ks_status }}<br />
                                    Z: {{ $order->Status }}
                                </p>
                            </td>
                            <td>
                                {{ $order->Name }}<br />
                                <p class="address">
                                    {{ $order->Address1 }}<br />
                                    {{ $order->State }}, {{ $order->Zip }}<br />
                                    {{ $order->Country }}
                                </p>
                                <p class="phone">{{ $order->Phone }}</p>
                                <p class="delivery-method">Delivery: {{ $order->DeliveryMethod }}</p>
                            </td>
                            <td>
                                @foreach ($order->LineItems as $item)
                                    @foreach ($item->PreviewFiles as $file)
                                        <img class="preview-image" src="{{ asset($file->path) }}" />
                                    @endforeach
                                @endforeach
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{ $orders->links() }}
    </div>
@endsection
