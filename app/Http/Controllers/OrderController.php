<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Log;
use Storage;
use Chumper\Zipper\Zipper;
use App\Jobs\FetchNewOrders;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.all', [
            'orders' => Order::whereNotNull('ks_status')->paginate(10),
        ]);
    }


    /**
     * Dispatch the Fetch New Orders job manually
     *
     * @return \Illuminate\Http\Response
     */
    public function fetchnew()
    {
        FetchNewOrders::dispatch();
        return redirect('/orders/new');
    }


    /**
     * Display a listing of ACCEPTED orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        return view('order.new', [
            'orders' => Order::where('status','ASSIGNED')->where('ks_status','ACCEPTED')->get(),
        ]);
    }


    /**
     * Display a listing of IN-PRODUCTION orders.
     *
     * @return \Illuminate\Http\Response
     */
    public function shipping()
    {
        return view('order.shipping', [
            'orders' => Order::where('ks_status','IN-PRODUCTION')->get(),
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        return view('order.edit', [
            'order' => $order,
            'items' => $order->lineItems,
        ]);
    }


    public function complete(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->ks_status = 'SHIPPED';
        $order->save();
        return redirect('/orders/shipping');
    }


    /**
     * Zip the Print and Preview files for supplied Orders
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function downloadPrintFiles(Request $request)
    {
        $ids = $request->ids;
        foreach ($ids as $id) {
            $order = Order::find($id);
            foreach ($order->LineItems as $item) {
                foreach ($item->PrintFiles as $print) {
                    if($print->path) $files[] = $print->path;
                }
            }
            $order->ks_status = 'IN-PRODUCTION';
            $order->save();
        }
        $zip = 'storage/zip/zazzle-print-files.zip';
        $zipper = new Zipper;
        $zipper->make($zip)->add($files)->close();
        return response()->download($zip)->deleteFileAfterSend(true);
    }

    /**
     * Update the specified Order with new shipping info
     * Fetch and download new shipping documents
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downloadShippingDocs(Request $request, $id)
    {

        $validatedData = $request->validate([
            'Weight' => 'required|numeric|min:0',
            'Length' => 'nullable|numeric|min:0',
            'Width' => 'nullable|numeric|min:0',
            'Height' => 'nullable|numeric|min:0'
        ]);

        $order = Order::findOrFail($id);
        $order->fill([
            'Weight' => $request->Weight,
            'Length' => $request->Length,
            'Width' => $request->Width,
            'Height' => $request->Height,
        ])->save();
        $order->fetchShippingDocs();
//        foreach ($order->shippingDocuments as $doc) {
//            if($doc->path) $files[] = $doc->path;
//        }
//        foreach ($order->packingSheets as $doc) {
//            if($doc->path) $files[] = $doc->path;
//        }
//        $zip = 'storage/zip/'.$order->OrderId.'-shipping-docs.zip';
//        $zipper = new Zipper;
//        $zipper->make($zip)->add($files)->close();
//        return response()->download($zip)->deleteFileAfterSend(true);
        return redirect('/orders/'.$id.'/edit');
    }
}
