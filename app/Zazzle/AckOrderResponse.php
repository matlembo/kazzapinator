<?php

namespace App\Services\Zazzle;

class AckOrderResponse
{

    private $status;

    public function __construct($response){
        $node = simplexml_load_string($response);
        $this->status = (string) $node->Status->Code;
    }

    public function getStatus(){
        return $this->status;
    }

}