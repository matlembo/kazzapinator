<?php

use Faker\Generator as Faker;

$factory->define(App\LineItem::class, function (Faker $faker) {
    return [
        'LineItemId' => $faker->numberBetween(1100001100014496391,1100001200014496391),
        // 'LineItemType',
        'Quantity' => 1,
        // 'Description',
        // 'Attributes',
        // 'ReprintInstructions',
        // 'VendorAttributes',
        // 'Price',
    ];
});
