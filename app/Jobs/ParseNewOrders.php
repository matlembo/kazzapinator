<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Services\Zazzle\ListNewOrdersResponse as Zazzle;
use Log;

class ParseNewOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $response;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Zazzle $response)
    {
        $this->response = $response;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Parse New Orders Job called.');
        $this->response->setXml();
        foreach ($this->response->parseData() as $order) {
            $order = Zazzle::storeData($order);
            $order->ackOrder();
        }
    }
}
