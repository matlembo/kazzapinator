z<?php

// PROCEDURES
// These are basically the Console Commands / Queued Jobs, right?
// Is there a place to put them so they can be invoked by either
// (not to mention a web request)?

function FetchNewOrders() {

  // Call ListNewOrders API method
  // Loop through returned orders, store and acknowledge each

}

function FetchUpdatedOrders() {

  // Call Zazzle ListUpdatedOrders method
  // Loop through returend orders and process updates

}

function GetShippingLabels() {

  // Retrieve Order(s) from DB
  // Ensure weight, dimension and format data are present
  // Call Zazzle getShippingLabel method
  // Process response (store shipping documents)

}

namespace App\Services\Zazzle;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use App\Order;

class Client {

  public function __construct()
  {
    $this->client = new GuzzleClient([
        'base_uri' => config('services.zazzle.base_uri'),
        'timeout' => 10.0,
    ]);
  }

  public function listNewOrders()
  {
    $response = new Zazzle\Response;
    // 
    return $response;
  }

  public function listUpdatedOrders()
  {

  }

  public function ackOrder(Order $order)
  {

  }

  public function getShippingLabel()
  {

  }

}

 

interface ZazzleResponse {

  public function __construct();

  // Save XML response to DB or filesystem
  // returns $id
  public function storeXml() : int;
  
  // Get XML from DB or filesystem
  // returns $xml
  public function retrieveXml(int $id);
  
  // Generator
  // Gives storeData someting to loop through
  public function parseData() : \Generator ; // like a Closure!

  // Takes parsed data and stores it as models (or whatever)
  public function storeData(); // No return, throw Exception on failure

}



trait SomeTraitName {

  private $xml;  

  public function __construct(String $xml)
  {
    $this->xml = $xml;
  }

  // Save XML response to DB or filesystem
  // returns $id
  public function storeXml()
  {

  }
  
  // Get XML from DB or filesystem
  // returns $xml
  public function retrieveXml(int $id)
  {
    
  }

}




class ListNewOrdersResponse implements ZazzleResponseContract {

  use ZazzleResponseTrait; // Traits override parent classes, but can be overridden locally.

  // Loop through returned orders, store and acknowledge

  public function __construct()
  {
    
  }

}

class ListUpdatedOrdersResponse implements ZazzleResponseContract {

  // Loop through returned orders, process updates and acknoledge

  public function __construct()
  {
    
  }

}

class GetShippingLabelResponse implements ZazzleResponseContract {

  public function __construct()
  {

  }

}





