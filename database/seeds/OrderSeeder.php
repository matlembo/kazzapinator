<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Order::class, 15)->create()->each(function ($order) {
          $qty = rand(1,3);
          $i = 1;
          while ($i <= $qty) {
            $lineItem = $order->lineItems()->save(factory(App\LineItem::class)->make());
            $previewFile = $lineItem->PreviewFiles()->save(factory(App\PreviewFile::class)->make());
            $previewFile->storeFile();
            $printFile = $lineItem->PrintFiles()->save(factory(App\PrintFile::class)->make());
            $printFile->storeFile();
            $i++;
          }
      });
    }
}
