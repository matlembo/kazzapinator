<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('OrderId')->nullable();
            $table->string('OrderDate')->nullable();
            $table->string('ShipByDate')->nullable();
            $table->string('OrderType')->nullable();
            $table->string('DeliveryMethod')->nullable();
            $table->string('Priority')->nullable();
            $table->string('Currency')->nullable();
            $table->string('Status')->nullable();
            $table->string('ks_status')->nullable();
            $table->string('Attributes')->nullable();
            $table->string('Address1')->nullable();
            $table->string('Address2')->nullable();
            $table->string('Address3')->nullable();
            $table->string('Name')->nullable();
            $table->string('Name2')->nullable();
            $table->string('City')->nullable();
            $table->string('State')->nullable();
            $table->string('Country')->nullable();
            $table->string('CountryCode')->nullable();
            $table->string('Zip')->nullable();
            $table->string('Phone')->nullable();
            $table->string('Email')->nullable();
            $table->string('Type')->nullable();
            $table->float('Weight')->nullable();
            $table->float('Height')->nullable();
            $table->float('Width')->nullable();
            $table->float('Length')->nullable();
            $table->string('Carrier')->nullable();
            $table->string('Method')->nullable();
            $table->string('TrackingNumber')->nullable();
            $table->boolean('IsReprint')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
