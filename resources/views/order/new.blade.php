@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="/orders/download-print-files" id="download-print-files-form">
            @csrf
            <div class="row">
                <div class="col">
                    <h1>New Orders</h1>
                </div>
                <div class="col text-right">
                    <div class="form-group">
                        @if($orders->count())
                            <input class="btn btn-primary" type="submit" value="Download Print Files for Selected Orders" />
                        @endif
                        <a href="/orders/fetchnew" class="btn btn-danger">Check for New Orders Now</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Order</th>
                            <th>Ship To</th>
                            <th>Items</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="ids[]" value="{{ $order->id }}" checked />
                                    </div>
                                </td>
                                <td>
                                    <a href="/orders/{{ $order->id }}/edit">{{ $order->OrderId }}</a>
                                    <p class="order-date">{{ \Carbon\Carbon::parse($order->OrderDate)->toDayDateTimeString() }}</p>
                                    <p>
                                        KS: {{ $order->ks_status }}<br />
                                        Z: {{ $order->Status }}
                                    </p>
                                </td>
                                <td>
                                    {{ $order->Name }}<br />
                                    <p class="address">
                                        {{ $order->Address1 }}<br />
                                        {{ $order->State }}, {{ $order->Zip }}<br />
                                        {{ $order->Country }}
                                    </p>
                                    <p class="phone">{{ $order->Phone }}</p>
                                    <p class="delivery-method">Delivery: {{ $order->DeliveryMethod }}</p>
                                </td>
                                <td>
                                    @foreach ($order->LineItems as $item)
                                        @foreach ($item->PreviewFiles as $file)
                                            <img class="preview-image" src="{{ asset($file->path) }}" />
                                        @endforeach
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
@endsection
