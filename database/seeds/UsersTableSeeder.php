<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Matt Lembo',
            'email' => 'matt.lembo@gmail.com',
            'password' => bcrypt('sixtimes9isKazzapinator'),
            'created_at' => Carbon::now(),
            'is_admin' => TRUE,
        ]);
        DB::table('users')->insert([
            'name' => 'Keyscaper Operations',
            'email' => 'operations@keyscaper.com',
            'password' => bcrypt('zazzlefullfillment'),
            'created_at' => Carbon::now(),
            'is_admin' => TRUE,
        ]);
    }
}
