<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Zazzle\Client;
use App\Services\Zazzle\MockClient\MockClient;

class ZazzleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ZazzleClient', function($app){
            if (config('services.zazzle.mock_client')) {
                return new MockClient();
            } else {
                return new Client();
            }
        });
    }
}
