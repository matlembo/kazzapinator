<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class PackingSheet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'PageNumber',
        'Type',
        'Description',
        'Url',
    ];


    /**
     * Get the Order that owns the Packing Sheet.
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


    /**
     * Store the file from Zazzle's URL to the filesystem
     */
    public function storeFile()
    {
        $contents = file_get_contents($this->Url);
        $name = 'packing-'.$this->PageNumber.'o'.$this->order->PackingSheets->count().'-'.$this->order->OrderId.'.'.explode('/',$this->Type)[1];
        Storage::put('public/PackingSheets/'.$name, $contents);
        $this->path = 'storage/PackingSheets/'.$name;
        $this->save();
    }
}
