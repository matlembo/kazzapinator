<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\Zazzle\Client;

class ZazzleFetchUpdatedOrdersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zazzle:fetchupdatedorders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs getUpdatedOrders API call and processes response.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        parent::__construct();
        $this->client = $client;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = $this->client->listUpdatedOrders();
        $this->info($response->getBody());
    }
}
