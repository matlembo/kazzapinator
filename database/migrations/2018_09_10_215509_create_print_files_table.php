<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('print_files', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('Type')->nullable();
            $table->string('Url')->nullable();
            $table->string('path')->nullable();
            $table->string('Description')->nullable();
            $table->unsignedInteger('line_item_id');
            $table->foreign('line_item_id')->references('id')->on('line_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('print_files');
    }
}
