<?php

use Illuminate\Database\Seeder;
use Illuminate\Filesystem\Filesystem;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        if (App::environment(['local','testing'])) {
          $filesystem = new Filesystem;
          $filesystem->cleanDirectory('storage/app/public');
          if (!config('services.zazzle.mock_client')) $this->call(OrderSeeder::class);
        }
    }
}
