<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;


class ShippingDocument extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Type',
        'Format',
        'Url'
    ];


    /**
     * Get the Order that owns the Shipping Document.
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


    /**
     * Store the file from Zazzle's URL to the filesystem
     */
    public function storeFile()
    {
        $contents = file_get_contents($this->Url);
        $name = $this->Type.'-'.$this->order->OrderId.'.'.strtolower($this->Format);
        Storage::put('public/ShippingDocument/'.$name, $contents);
        $this->path = 'storage/ShippingDocument/'.$name;
        $this->save();
    }

}
