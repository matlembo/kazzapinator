<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class PreviewFile extends Model
{

    /**
     * Get the Line Item that owns Preview File.
     */
    public function lineItem()
    {
        return $this->belongsTo('App\LineItem');
    }

    /**
     * Store the file from Zazzle's URL to the filesystem
     */
    public function storeFile()
    {
        $contents = file_get_contents($this->Url);
        $name = 'preview-'.$this->lineItem->LineItemId.'.'.explode('/',$this->Type)[1];
        Storage::put('public/PreviewFiles/'.$name, $contents);
        $this->path = 'storage/PreviewFiles/'.$name;
        $this->save();
    }

}
