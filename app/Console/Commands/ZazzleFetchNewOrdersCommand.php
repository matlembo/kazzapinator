<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\FetchNewOrders;
use App\Services\Zazzle\Client;

class ZazzleFetchNewOrdersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zazzle:fetchneworders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatches FetchNewOrders Job.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $job = new FetchNewOrders;
        $job->dispatch();
    }
}
