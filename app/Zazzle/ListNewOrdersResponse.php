<?php

namespace App\Services\Zazzle;

use App\Services\Zazzle\Client;
use App\Order;
use App\LineItem;
use App\PrintFile;
use App\PreviewFile;
use App\PackingSheet;
use \XMLReader;
use \SimpleXMLElement;
use Storage;

class ListNewOrdersResponse implements Contracts\ZazzleResponse
{

    use Concerns\HandlesZazzleResponse;

    /**
     * @return \Generator
     */
    public function parseData()
    {

        $reader = new XMLReader();
        $reader->XML($this->xml);

        while ($reader->read() && $reader->name != 'Order');
        while ($reader->name === 'Order') {

            $xml = $reader->readOuterXML();
            $node = simplexml_load_string($xml);

            $order = new Order;

            $order->OrderId         = $node->OrderId;
            $order->OrderDate       = $node->OrderDate;
            $order->OrderType       = $node->OrderType;
            $order->DeliveryMethod  = $node->DeliveryMethod;
            $order->Priority        = $node->Priority;
            $order->Currency        = $node->Currency;
            $order->Status          = $node->Status;
            $order->Attributes      = $node->Attributes;

            $order->Address1        = $node->ShippingAddress->Address1;
            $order->Address2        = $node->ShippingAddress->Address2;
            $order->Address2        = $node->ShippingAddress->Address2;
            $order->Address3        = $node->ShippingAddress->Address3;
            $order->Name            = $node->ShippingAddress->Name;
            $order->Name2           = $node->ShippingAddress->Name2;
            $order->City            = $node->ShippingAddress->City;
            $order->State           = $node->ShippingAddress->State;
            $order->Country         = $node->ShippingAddress->Country;
            $order->Zip             = $node->ShippingAddress->Zip;
            $order->Phone           = $node->ShippingAddress->Phone;
            $order->Email           = $node->ShippingAddress->Email;
            $order->Type            = $node->ShippingAddress->Type;

            $lineItems = $node->LineItems->children();
            foreach ($lineItems as $item) {
                $lineItem = new LineItem;
                $lineItem->LineItemId       = $item->LineItemId;
                $lineItem->OrderId          = $item->OrderId;
                $lineItem->LineItemType     = $item->LineItemType;
                $lineItem->Quantity         = $item->Quantity;
                $lineItem->Description      = $item->Description;
                $lineItem->Attributes       = $item->Attributes;
                $lineItem->VendorAttributes = $item->VendorAttributes;
                $lineItem->Price            = $item->Price;
                $lineItem->ProductId        = $item->ProductId;

                $printFiles = $item->PrintFiles->children();
                foreach ($printFiles as $file) {
                    $printFile = new PrintFile;
                    $printFile->Type          = $file->Type;
                    $printFile->Url           = $file->Url;
                    $printFile->Description   = $file->Description;
                    $lineItem->PrintFiles->add($printFile);
                }

                $previewFiles = $item->Previews->children();
                foreach ($previewFiles as $file) {
                    $previewFile = new PreviewFile;
                    $previewFile->Type          = $file->Type;
                    $previewFile->Url           = $file->Url;
                    $previewFile->Description   = $file->Description;
                    $lineItem->PreviewFiles->add($previewFile);
                }

                $order->LineItems->add($lineItem);
            }

            $packingSheets = $node->PackingSheet->children();
            foreach ($packingSheets as $page) {
                $packingSheet = new PackingSheet;
                $packingSheet->PageNumber     = $page->PageNumber;
                $packingSheet->Type           = $page->Front->Type;
                $packingSheet->Description    = $page->Front->Description;
                $packingSheet->Url            = $page->Front->Url;
                $order->PackingSheets->add($packingSheet);
            }

            yield $order;

            $reader->next('Order');

        }

        $reader->close();

    }


    /**
     * @param App\Order $order
     */
    public static function storeData(Order $order)
    {

        $order->save();

        foreach ($order->LineItems as $lineItem) {

            $order->LineItems()->save($lineItem);

            foreach ($lineItem->PrintFiles as $printFile) {
                $lineItem->PrintFiles()->save($printFile);
                $printFile->storeFile();
            }

            foreach ($lineItem->PreviewFiles as $previewFile) {
                $lineItem->PreviewFiles()->save($previewFile);
                $previewFile->storeFile();
            }

        }

        foreach ($order->PackingSheets as $packingSheet) {
            $order->PackingSheets()->save($packingSheet);
            $packingSheet->storeFile();
        }

        return $order;

    }



}