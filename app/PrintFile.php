<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class PrintFile extends Model
{

    /**
     * Get the Line Item that owns Print File.
     */
    public function lineItem()
    {
        return $this->belongsTo('App\LineItem');
    }

    /**
     * Store the file from Zazzle's URL to the filesystem
     */
    public function storeFile()
    {
        $contents = file_get_contents($this->Url);
        $name = $this->lineItem->LineItemType.'-'.$this->lineItem->OrderId.'-'.$this->lineItem->LineItemId.'.'.explode('/',$this->Type)[1];
        Storage::put('public/PrintFiles/'.$name, $contents);
        $this->path = 'storage/PrintFiles/'.$name;
        $this->save();
    }
}
