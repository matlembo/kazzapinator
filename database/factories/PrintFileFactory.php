<?php

use Faker\Generator as Faker;

$factory->define(App\PrintFile::class, function (Faker $faker) {
    return [
        'Url' => $faker->imageUrl(),
    ];
});
