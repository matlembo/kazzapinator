<?php

namespace App\Services\Zazzle\MockClient;

use App\Services\Zazzle\AckOrderResponse;
use App\Services\Zazzle\GetShippingLabelResponse;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Filesystem\Filesystem;
use App\Services\Zazzle\ListNewOrdersResponse;
use App\Order;

class MockClient
{

    private $listNewOrdersXML;

    private $listUpdatedOrdersXML;

    private $ackOrderXML;

    private $getShippingLabelXML;

    /**
     * Create a new Zazzle Client.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri' => config('services.zazzle.base_uri'),
            'timeout' => 10.0,
        ]);
    }

    /**
     * Mocks the Zazzle API's ListNewOrders method by returning static XML
     *
     * @return ListNewOrdersResponse
     */
    public function listNewOrders()
    {
        $file = new Filesystem();
        $xml = $file->get(__DIR__.'/listNewOrders.xml');
        $this->listNewOrdersXML = $xml;
        return new ListNewOrdersResponse($this->listNewOrdersXML);
    }

    /**
     * Mocks the Zazzle API's ListUpdatedOrders method by returning static XML
     *
     * @return String  xml
     */
    public function listUpdatedOrders()
    {
        $file = new Filesystem();
        $xml = $file->get(__DIR__.'/listUpdatedOrders.xml');
        $this->listUpdatedOrdersXML = $xml;
        return new ListUpdatedOrdersResponse($this->listUpdatedOrdersXML);
    }

    /**
     * Mocks the Zazzle API's AckOrder method by returning static XML
     *
     * @return String  HTTP Response Code
     */
    public function ackOrder(Order $order)
    {
        $file = new Filesystem();
        $xml = $file->get(__DIR__.'/ackOrder.xml');
        $this->ackOrderXML = $xml;
        return new AckOrderResponse($this->ackOrderXML);
    }

    /**
     * Mocks the Zazzle API's ShippingLabel method by returning static XML
     *
     * @return String  xml
     */
    public function getShippingLabel(Order $order)
    {
        $file = new Filesystem();
        $xml = $file->get(__DIR__.'/getShippingLabel.xml');
        $this->getShippingLabelXML = $xml;
        return new GetShippingLabelResponse($this->getShippingLabelXML);
    }


}