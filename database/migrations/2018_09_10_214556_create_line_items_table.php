<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->bigInteger('LineItemId')->nullable();
            $table->bigInteger('OrderId')->nullable(); // FK?
            $table->string('LineItemType')->nullable();
            $table->string('Quantity')->nullable();
            $table->string('Description')->nullable();
            $table->string('Attributes')->nullable();
            $table->string('ReprintInstructions')->nullable();
            $table->string('VendorAttributes')->nullable();
            $table->string('Price')->nullable(); // Not in Zazzle Documentation Definition table but is in example response?
            $table->string('ProductId')->nullable();
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_items');
    }
}
