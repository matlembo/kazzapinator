<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'zazzle' => [
        'base_uri' => env('ZAZZLE_BASE_URI','https://vendor.Zazzle.com/v100/api.aspx'),
        'vendor_id' => env('ZAZZLE_VENDOR_ID'),
        'secret_key' => env('ZAZZLE_SECRET_KEY'),
        'shipping_label_format' => env('ZAZZLE_SHIPPING_LABEL_FORMAT','ZPL'),
        'mock_client' => env('ZAZZLE_MOCK_CLIENT',TRUE),
        'start_time' => env('ZAZZLE_START_TIME','1:00'),
        'end_time' => ENV('ZAZZLE_END_TIME','23:00'),
    ],

];
