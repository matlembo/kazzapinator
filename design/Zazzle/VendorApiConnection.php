<?php

namespace App\Services\Zazzle;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use App\Order;

class VendorApiConnection {


  public function __construct(){
    $this->client = new GuzzleClient([
        'base_uri' => config('services.zazzle.base_uri'),
        'timeout' => 10.0,
    ]);
  }


  /**
   * @param String xml response from Zazzle API
   * 
   * @return Array
   */
  private function xmlToArray($body)
  {
    $xml = simplexml_load_string($input);
    // Remove empty nodes, otherwise they get parsed into
    // empty arrays and break mass assignment model creation
    $xpath = '//*[not(normalize-space())]';
    foreach (array_reverse($xml->xpath($xpath)) as $remove) {
      unset($remove[0]);
    }
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);
    return $array;
  }


  /**
   * @param Array  $input Parsed response from Zazzle ListNewOrder method
   *
   * @return \App\Order
   */
  private function parseOrderResponse(Array $input)
  {
        
    
    $order = Order::create($input);
    $order->shippingAddress()->create($input['ShippingAddress']);
    if (isset($input['LineItems'])) foreach ($input['LineItems'] as $item) {
      $lineItem = $order->lineItems()->create($item);
      if (isset($item['PrintFiles'])) foreach ($item['PrintFiles'] as $file) {
        $lineItem->printFiles()->create($file);
      }
      if (isset($item['Previews'])) foreach ($item['Previews'] as $file) {
        $lineItem->previewFiles()->create($file);
      }
    }
    if (isset($input['Products'])) foreach ($input['Products'] as $item) {
      $order->products()->create($item);
    }
    if (isset($item['PackingSheet'])) foreach ($input['PackingSheet'] as $item) {
      $order->packingSheet()->create($item);
    }

    // run received method

    return $order;

  }


  public function getNewOrders()
  {

    Log::info('Calling Zazzle ListNewOrders Method');
    
    $code = $response->getStatusCode(); // 200
    $reason = $response->getReasonPhrase(); // OK
    $body = $response->getBody();

    // Check for HTTP error

    
    // All good, parse response
    $data = $this->xmlToArray($body);
    
    // Check for API error
    if ($data['Status']['Code'] == 'ERROR') {
      $error = 'ListNewOrders Error! '.$data['Status']['Info'];
      Log::error($error);
      return $error;
    }

    // All good, parse orders into the DB
    if (isset($data['Result']['Orders'])) {
      $orders = $data['Result']['Orders'];  
      foreach ($orders as $input) {
        $order = parseOrder($input);
        $order->acknowledge();
      }
    } else {
      Log::info('No new orders');
      return 'No new orders';
    }
  
  }

    



}