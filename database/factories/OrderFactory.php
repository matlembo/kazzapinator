<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    return [
        'OrderId' => $faker->numberBetween(2500001100014496391,2500001200014496391), // 2500001100014496391
        'OrderDate' => $faker->iso8601(), // 2019-06-10T04:41:20.492
        'ShipByDate' => $faker->iso8601(), // 2019-06-10T04:41:20.492
        'OrderType' => 'zazzle_default',  // zazzle_default
        'DeliveryMethod' => $faker->randomElement(['standard','premium']), // standard, premium
        'Priority' => $faker->randomElement(['Low','Normal','High']), // Low, Normal, High
        'Status' => $faker->randomElement(['ASSIGNED','ACCEPTED','SHIPPED','CANCELLED']), // ASSIGNED, ACCEPTED, SHIPPED, CANCELLED
        // 'IsReprint', // (bool)
        // 'ReprintReason', // Color Accuracy Issues
        // 'OriginalOrderId', // 2500001100014496391
        'Address1' => $faker->streetAddress(),
        // 'Address2',
        // 'Address3',
        'Name' => $faker->name(),
        // 'Name2',
        'City' => $faker->city(),
        'State' => $faker->stateAbbr(),
        'Country' => $faker->country(),
        // 'CountryCode',
        'Zip' => $faker->postcode(),
        'Phone' => $faker->phoneNumber(),
        'Email' => 'maker.management@zazzle.com',
        'Type' => $faker->randomElement(['Residential','PO','APO','Zazzle']), // Residental, PO, APO, Zazzle
        // 'Weight',
        // 'Length',
        // 'Width',
        // 'Height',
        // 'Carrier' => $faker->randomElement(['UPS','FedEx']), // UPS, FedEx
        // 'Method' => 'UPS Worldwide Expedited', // UPS Worldwide Expedited
        // 'TrackingNumber' => $faker->randomNumber(22)
    ];
});
