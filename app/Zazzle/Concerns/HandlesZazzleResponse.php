<?php

namespace App\Services\Zazzle\Concerns;

use App\RawResponse;

trait HandlesZazzleResponse {

    /**
    * The body of the response
    *
    * @var string (xml)
    */
    private $xml;

    /**
    * The ID of the stored data
    *
    * @var string
    */
    private $id;

    /**
     * Zazzle Response Status
     *
     * @var string
     */
    private $status;

    public function __construct($response){
        $this->xml = $response;
    }

    /**
    * Stores a copy of the response body to the DB
    */
    public function storeXml()
    {
        $rawResponse = new RawResponse;
        $rawResponse->data = $this->xml;
        $rawResponse->save();
        $this->id = $rawResponse->id;
    }

    /**
    * Sets the data property from the stored response body
    */
    public function setXml()
    {
        $rawResponse = RawResponse::findOrFail($this->id);
        $this->xml = $rawResponse->data;
    }

    /**
     * Gets the data property
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * Gets the data property
     */
    public function getId()
    {
        return $this->id;
    }

}