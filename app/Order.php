<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Services\Zazzle\GetShippingLabelResponse as ZazzleResponse;
use Log;

class Order extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'OrderId',
        'OrderDate',
        'ShipByDate',
        'OrderType',
        'DeliveryMethod',
        'Priority',
        'Status',
        'IsReprint',
        'ReprintReason',
        'OriginalOrderId',
        'OrderId',
        'Address1',
        'Address2',
        'Address3',
        'Name',
        'Name2',
        'City',
        'State',
        'Country',
        'CountryCode',
        'Zip',
        'Phone',
        'Email',
        'Type',
        'Weight',
        'Length',
        'Width',
        'Height',
        'Carrier',
        'Method',
        'TrackingNumber'
    ];


    /**
     * Get the Line Items for the Order.
     */
    public function lineItems()
    {
        return $this->hasMany('App\LineItem');
    }


    /**
     * Get the Packing Sheet Files for the Order.
     */
    public function packingSheets()
    {
        return $this->hasMany('App\PackingSheet');
    }


    /**
     * Get all of the Print Files for the Order.
     */
    public function printFiles()
    {
        return $this->hasManyThrough('App\PrintFile', 'App\LineItem');
    }


    /**
     * Get all of the Preview Files for the Order.
     */
    public function previewFiles()
    {
        return $this->hasManyThrough('App\PreviewFile', 'App\LineItem');
    }

    /**
     * Get all of the Shipping Documents for the Order.
     */
    public function shippingDocuments()
    {
        return $this->hasMany('App\ShippingDocument');
    }


    /**
     * Get the Dimension String to send with the GetShippingLabel API call
     */
    public function dimensionString()
    {
        return 'h='.$this->Height.'&w='.$this->Width.'&l='.$this->Length;
    }


    /**
     * Send Zazzle an AckOrder message via API
     */
    public function ackOrder()
    {
        $client = resolve('ZazzleClient');
        $response = $client->ackOrder($this);
        if ($response->getStatus() === 'SUCCESS') {
            $this->ks_status = 'ACCEPTED';
            $this->save();
        }
    }


    /**
     * Fetch Shipping Documents from Zazzle API
     */
    public function fetchShippingDocs()
    {
        $client = resolve('ZazzleClient');
        $response = $client->getShippingLabel($this);
        $response->storeXML();
        $this->shippingDocuments()->delete();
        foreach ($response->parseData() as $doc) {
            ZazzleResponse::storeData($doc,$this);
            $doc->storeFile();
        }
    }

}
