<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LineItem extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LineItemId',
        'OrderId',
        'LineItemType',
        'Quantity',
        'Description',
        'Attributes',
        'ReprintInstructions',
        'VendorAttributes',
        'Price',
    ];


    /**
     * Get the Order that owns the Line Item.
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }


    /**
     * Get the Print File for the Line Item.
     */
    public function printFiles()
    {
        return $this->hasMany('App\PrintFile');
    }


    /**
     * Get the Print File for the Line Item.
     */
    public function previewFiles()
    {
        return $this->hasMany('App\PreviewFile');
    }


}
