@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row">
        <div class="col">
            <h1>Zazzle Order # {{ $order->OrderId }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            Zazzle Status: {{ $order->Status }}
        </div>
        <div class="col">
            Keyscaper Status: {{ $order->ks_status }}
        </div>
    </div>
    
    <hr />
    <div class="row">
        <div class="col">
            <h2>Ship To</h2>
        </div>
        <div class="col">
            <p class="address">
                <h4>{{ $order->Name}}</h4>
                {{ $order->Address1 }}<br />
                @if($order->Address2) {{ $order->Address2 }}<br />@endif
                @if($order->Address3) {{ $order->Address3 }}<br />@endif
                {{ $order->City }}, {{ $order->State}} {{ $order->Zip}}<br />
                {{ $order->Country}}
            </p>
        </div>
    </div>

    <hr />
    <div class="row">
        <div class="col">
            <h2>Shipping Documents</h2>
            @foreach($order->shippingDocuments as $doc)
                <a href="{{ $doc->Url }}" target="_blank">{{ $doc->Type }}</a><br />
            @endforeach
            <?php $i = 1; ?>@foreach($order->packingSheets as $doc)
                <a href="{{ $doc->Url }}" target="_blank">Packing Sheet {{ $i .' of '. $order->packingSheets->count() }}</a><br />
                <?php $i++; ?>
            @endforeach
        </div>
        <form method="POST" action="/orders/{{ $order->id }}/download-shipping-docs">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="Weight">Weight (lbs.) *</label>
                        <input required type="number" min="0" class="form-control @if($errors->has('Weight')) {{ 'is-invalid' }} @endif" name="Weight" placeholder="" value="{{ $order->Weight }}">
                        @if($errors->has('Weight'))<div class="invalid-feedback">{{ $errors->first('Weight') }}</div>@endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="Length">Length (in.)</label>
                        <input type="number" min="0" class="form-control @if($errors->has('Length')) {{ 'is-invalid' }} @endif" name="Length" placeholder="" value="{{ $order->Length }}">
                        @if($errors->has('Length'))<div class="invalid-feedback">{{ $errors->first('Length') }}</div>@endif
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="Width">Width (in.)</label>
                        <input type="number" min="0" class="form-control @if($errors->has('Width')) {{ 'is-invalid' }} @endif" name="Width" placeholder="" value="{{ $order->Width }}">
                        @if($errors->has('Width'))<div class="invalid-feedback">{{ $errors->first('Width') }}</div>@endif
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="Height">Height (in.)</label>
                        <input type="number" min="0" class="form-control @if($errors->has('Height')) {{ 'is-invalid' }} @endif" name="Height" placeholder="" value="{{ $order->Height }}">
                        @if($errors->has('Height'))<div class="invalid-feedback">{{ $errors->first('Height') }}</div>@endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Fetch Shipping Documents</button>
                </div>
                <div class="col text-right">
                    <a class="btn btn-danger" href="/orders/{{ $order->id }}/complete">Mark Order Shipped</a>
                </div>
            </div>
        </form>
    </div>

    <hr />
    <div class="row">
        <div class="col">
            <h2>Line Items</h2>
        </div>
        <div class="col text-right">
            <form method="POST" action="/orders/download-print-files" id="download-print-files-form">
                @csrf
                <input type="hidden" name="ids[]" value="{{ $order->id }}" />
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" value="Download All Print Files" />
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Zazzle Line Item ID</th>
                  <th>Quantity</th>
                  <th>Previews</th>
                  <th>Print Files</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($items as $item)
                  <tr>
                    <td>{{ $item->LineItemId }}</td>
                    <td>{{ $item->Quantity }}</td>
                    <td>
                        @foreach ($item->previewFiles as $file)
                            <img class="preview-image" src="{{ $file->Url }}" />
                        @endforeach
                    </td>
                    <td>
                        @foreach ($item->printFiles as $file)
                            <a target="_blank" href="{{ $file->Url }}">Download Print File</a>
                        @endforeach
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
        </div>
    </div>

</div>
@endsection
