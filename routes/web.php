<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {

    Route::get('/orders', 'OrderController@index')->name('orders.index');
    Route::get('/orders/{id}/edit', 'OrderController@edit')->name('orders.edit');
    Route::get('/orders/new', 'OrderController@new')->name('orders.new');
    Route::get('/orders/shipping', 'OrderController@shipping')->name('orders.shipping');
    Route::post('/orders/{id}/download-shipping-docs', 'OrderController@downloadShippingDocs');
    Route::get('/orders/{id}/complete', 'OrderController@complete');
    Route::post('/orders/download-print-files', 'OrderController@downloadPrintFiles');
    Route::get('/orders/fetchnew', 'OrderController@fetchnew');

});