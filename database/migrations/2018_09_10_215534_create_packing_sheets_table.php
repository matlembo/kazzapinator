<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackingSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packing_sheets', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->smallInteger('PageNumber')->nullable();
            $table->string('Type')->nullable();
            $table->string('Description')->nullable();
            $table->string('Url')->nullable();
            $table->string('path')->nullable();
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packing_sheets');
    }
}
