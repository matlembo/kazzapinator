<?php

namespace App\Services\Zazzle;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\MessageFormatter;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use App\Services\Zazzle\ListNewOrdersResponse;
use App\Services\Zazzle\ListUpdatedOrdersResponse;
use App\Services\Zazzle\GetShippingLabelResponse;
use App\Services\Zazzle\AckOrderResponse;
use App\Order;
use Log;

class Client
{

    /**
     * Create a new Zazzle Client.
     *
     * @return void
     */
    public function __construct()
    {

        $logger = new Logger('GuzzleLog');
        $logger->pushHandler(new RotatingFileHandler(storage_path().'/logs/zazzle-request.log',28,Logger::DEBUG));

        $stack = HandlerStack::create();
        $stack->push(
            Middleware::log(
                $logger,
                new MessageFormatter('{request}')
            )
        );

        $this->client = new GuzzleClient([
            'base_uri' => config('services.zazzle.base_uri'),
            'timeout' => 10.0,
            'handler' => $stack
        ]);
    }

    /**
     * Calls the Zazzle API's ListNewOrders method
     *
     * @return ListNewOrdersResponse
     */
    public function listNewOrders()
    {
        $response = $this->client->get('api.aspx', [
            'query' => [
                'method' => 'listneworders',
                'vendorid' => config('services.zazzle.vendor_id'),
                'hash' => md5(config('services.zazzle.vendor_id') . config('services.zazzle.secret_key')),
            ],
        ]);
        return new ListNewOrdersResponse($response->getBody());
    }

    /**
     * Calls the Zazzle API's ListUpdatedOrders method
     *
     * @return String  xml
     */
    public function listUpdatedOrders()
    {
        $response = $this->client->get('api.aspx', [
            'query' => [
                'method' => 'listupdatedorders',
                'vendorid' => config('services.zazzle.vendor_id'),
                'hash' => md5(config('services.zazzle.vendor_id') . config('services.zazzle.secret_key')),
            ],
        ]);
        return new ListUpdatedOrdersResponse($response->getBody());
    }

    /**
     * Calls the Zazzle API's AckOrder method
     *
     * @return String  HTTP Response Code
     */
    public function ackOrder(Order $order)
    {
        $response = $this->client->get('api.aspx', [
            'query' => [
                'method' => 'ackorder',
                'vendorid' => config('services.zazzle.vendor_id'),
                'orderid' => (string) $order->OrderId,
                'type' => 'new',
                'action' => 'accept',
                'hash' => md5(config('services.zazzle.vendor_id') . $order->OrderId . 'new' . config('services.zazzle.secret_key')),
            ],
        ]);
        Log::Debug('ackOrder Response: '.$response->getBody());
        return new AckOrderResponse($response->getBody());
    }

    /**
     * Calls the Zazzle API's ShippingLabel method
     *
     * @return String  xml
     */
    public function getShippingLabel(Order $order)
    {
        $response = $this->client->get('api.aspx', [
            'query' => [
                'method' => 'getshippinglabel',
                'vendorid' => config('services.zazzle.vendor_id'),
                'orderid' => (string) $order->OrderId,
                'weight' => (string) $order->Weight,
                'dimension' => (string) $order->dimensionString(),
                'format' => config('services.zazzle.shipping_label_format'),
                'hash' => md5(config('services.zazzle.vendor_id') . $order->OrderId . $order->Weight . config('services.zazzle.shipping_label_format') . config('services.zazzle.secret_key')),
            ],
        ]);
        Log::Debug('getShipping Response: '.$response->getBody());
        return new GetShippingLabelResponse($response->getBody());
    }


}