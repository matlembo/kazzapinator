<?php

namespace App\Services\Zazzle;
use App\ShippingDocument;
use App\Order;
use \XMLReader;

class GetShippingLabelResponse implements Contracts\ZazzleResponse
{

    use Concerns\HandlesZazzleResponse;

    /**
     * @return \Generator
     */
    public function parseData()
    {
        $reader = new XMLReader();
        $reader->XML($this->xml);

        while ($reader->read() && $reader->name != 'ShippingDocument');
        while ($reader->name === 'ShippingDocument') {

            $xml = $reader->readOuterXML();
            $node = simplexml_load_string($xml);
            $doc = new ShippingDocument;

            $doc->Type    = $node->Type;
            $doc->Format  = $node->Format;
            $doc->Url     = $node->Url;

            yield $doc;

            $reader->next('ShippingDocument');

        }

        $reader->close();

    }


    /**
     * @param App\ShippingDocument $doc
     */
    public static function storeData(ShippingDocument $doc, Order $order)
    {
        $order->shippingDocuments()->save($doc);
    }


}